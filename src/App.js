import {Lightning} from '@lightningjs/sdk';
import InputField from "./components/InputField";
import Keyboard from "./components/Keyboard";
import Results from "./components/Results";

export default class App extends Lightning.Component {
  static _template() {
    return {
      Background: {
        w: 1920, h: 1080, rect: true, color: 0xff0b111e, visible: true
      },
      InputField: {
        type: InputField, maxLength: 20, placeholder: "Search", x: 960, y: 50
      },
      Keyboard: {
        type: Keyboard, y: 200, x: 150,
        config: {
          layout: {
            abc: ['123', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
              'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'x', 'shift'],
            '123': ['abc', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'],
          },
          actions: ['Clear', 'Space', 'Delete'],
          def: 'abc'
        },
        signals: {onKey: true}
      },
      Results: {
        type: Results, y: 480, x: 80, clipping: true,
        w: 1920, h: 600
      }
    };
  }

  _init() {
    super._init();
    this._setState("Keyboard");
  }

  search(q) {
    // @todo query Api for search results
    this.tag("Results").items = [];
  }

  static _states() {
    return [
      class Input extends this {

      },
      class Keyboard extends this {
        onKey({value}) {
          if (value.length === 1) {
            this.tag("InputField").feed(value);

            // if searchfield value beyond 2
            // start searching
            if (this.value.length > 2) {
              this.search(this.value);
            }

          } else {
            const action = value.toLowerCase();

            if (this.tag("InputField")._hasMethod(action)) {
              this.tag("InputField")[action]();
            }
          }
        }

        _getFocused() {
          return this.tag("Keyboard");
        }

        _handleDown(){
          console.log("s:",this.tag("Results").isFocusable)
          if(this.tag("Results").isFocusable){
            this._setState("Results");
          }
        }
      },
      class Results extends this {
        _getFocused() {
          return this.tag("Results");
        }

        _handleUp(){
          this._setState("Keyboard");
        }
      }
    ];
  }

  get value() {
    return this.tag("InputField").value;
  }
}
