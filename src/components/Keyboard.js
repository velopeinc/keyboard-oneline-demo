import {Lightning} from '@lightningjs/sdk';
import Button from "./Button";

export default class Keyboard extends Lightning.Component {

    static _template() {
        return {
            Wrapper:{x: 800,
                flex:{justifyContent: 'space-around', wrap: true},
                Characters:{
                    flex:{direction:'row'}
                },
                Actions:{
                    flex:{direction:'row', justifyContent:'space-between', paddingTop:60}
                }
            }
        };
    }

    _init() {
        this._cidx = 0;
        this._aidx = 0;
        this._setState("Characters");
    }

    get active() {
        return this.children[this.index];
    }

    set config(v){
        const {layout, actions, def} = v;
        this._layout = layout;

        this.create("Characters", this._layout[def]);
        this.create("Actions", actions);
    }

    // reusable for layout switch
    create(ref, chars){
        let flexItem = {};
        if(ref === "Actions"){
            flexItem.marginLeft = 40
        }
        this.tag(ref).children = chars.map((label)=>{
            return {type: Button, label, flexItem}
        });
    }

    _handleLeft(){
        const index = this.index;
        const len = this.children.length;
        let newIndex = index - 1;

        if(newIndex < 0){
            newIndex = len - 1;
        }

        this.index = newIndex;
    }

    _handleRight(){
        const index = this.index;
        const len = this.children.length;
        let newIndex = index + 1;

        if(newIndex > len - 1){
            newIndex = 0;
        }

        this.index = newIndex;
    }

    _handleEnter(){
        const label = this.active.label;

        if(label === "shift"){
            if(this.state === "Characters.Upper"){
                this._setState("Characters");
                this.children.forEach((el)=>{
                    el.label = el.label.toLowerCase();
                });
            }else{
                this._setState("Characters.Upper");
            }
        }else if(label === "123" || label === "abc"){
            this.create("Characters", this._layout[label]);
        }else{
            this.signal("onKey",{value:label});
        }
    }

    _getFocused(){
        return this.active;
    }

    static _states(){
        return [
            class Characters extends this{

                set index(v) {
                    this._cidx = v;
                }

                get index() {
                    return this._cidx;
                }

                get children(){
                    return this.tag("Characters").children;
                }

                _handleDown(){
                    this._setState("Actions");
                }

                static _states(){
                    return [
                        class Upper extends this{
                            $enter(){
                                this.children.forEach((el)=>{
                                    el.label = el.label.length === 1 ? el.label.toUpperCase() : el.label
                                })
                            }
                        }
                    ]
                }
            },
            class Actions extends this{
                set index(v) {
                    this._aidx = v;
                }

                get index() {
                    return this._aidx;
                }

                get children(){
                    return this.tag("Actions").children;
                }

                _handleUp(){
                    this._setState("Characters");
                }
            }
        ]
    }
}