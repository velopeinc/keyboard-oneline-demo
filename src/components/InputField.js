import {Lightning} from '@lightningjs/sdk';

export default class InputField extends Lightning.Component{
    static _template(){
        return {
            Label:{ mountX:0.5,
                text: { textColor: 0x50ffffff, fontSize:60}
            }
        }
    }

    _init(){
        this._query = '';
    }

    feed(char){
        this.update({query:`${this._query}${char}`})
    }

    set maxLength(v){
        this._maxLength = parseInt(v) || 20;
    }

    set placeholder(v){
        this._placeholder = `${v}`;
        this.tag("Label").text = this._placeholder;
    }

    update({query=this._query}){
        this._query = query;
        let output = query;

        if(output.length > this._maxLength){
            const offset = output.length - this._maxLength;
            output = `...${output.substring(offset, output.length)}`;
        }

        this.patch({
            Label:{
                text:{text:query.length > 0 ? `"${output.toUpperCase()}"`: `${this._placeholder || ""}`}
            }
        });

    }

    clear(){
        this._query = "";
        this.update({});
    }

    delete(){
        this.update({query: this._query.substring(0, this._query.length - 1)})
    }

    space(){
        this.feed(" ");
    }

    get value(){
        return this._query;
    }
}