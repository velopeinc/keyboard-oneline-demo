import { Lightning } from '@lightningjs/sdk';
import List from "./List";

// @todo: remove when hooked up to real data
const createDummyItems = (amount=20, offset=40) =>{
    return new Array(amount).fill('').map((el, idx)=>{
        return { src:`https://picsum.photos/id/${idx+offset}/410/230`}
    });
}

export default class Results extends Lightning.Component{
    static _template(){
        return {
            Wrapper:{
                x: 20, y: 20
            }
        }
    }

    set items(v){
        this.tag("Wrapper").children = [
            {type: List, items: createDummyItems(20,20), title:"Category 1"},
            {type: List, items: createDummyItems(18,40), y: 400, title:"Category 2"},
            {type: List, items: createDummyItems(15,2), y: 800, title:"Category 3"}
        ];

        this._index = 0;
    }

    get isFocusable(){
        return this.lists.length;
    }

    get lists(){
        return this.tag("Wrapper").children;
    }

    _handleUp(){
        let idx = this._index;
        if(idx > 0){
            this._setIndex(--idx);
        }else{
            return false
        }
    }

    _handleDown(){
        let idx = this._index;
        this._setIndex(Math.min(++idx, this.lists.length - 1));
    }

    _setIndex(index){
        this.tag("Wrapper").setSmooth("y", 0 - index * 400 + 20)
        this._index = index;
    }

    _getFocused(){
        return this.lists[this._index];
    }

}

