import {Lightning} from '@lightningjs/sdk';
import ResultItem from "./ResultItem.js";

export default class List extends Lightning.Component{

    static _template(){
        return {
            Title:{
               text:{}
            },
            List:{
                y:100
            }
        }
    }

    _init(){
        this._index = 0;
    }

    set items(v){
        this.tag("List").children = v.map((data, idx)=>{
            return {type: ResultItem, x: idx*460, index:idx+1, data}
        });
    }

    set title(v){
        this._title = v;
        this.tag("Title").text.text = v;
    }

    get items(){
        return this.tag("List").children;
    }

    _handleLeft(){
        let idx = this._index;
        this._setIndex(Math.max(--idx, 0));
    }

    _handleRight(){
        let idx = this._index;
        this._setIndex(Math.min(++idx, this.items.length - 1));
    }

    _setIndex(index){
        this.tag("List").setSmooth("x", 0 - index * 460)
        this._index = index;
    }

    _getFocused(){
        return this.items[this._index];
    }
}