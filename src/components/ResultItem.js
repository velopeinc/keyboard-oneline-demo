import { Lightning } from '@lightningjs/sdk';

export default class ResultItem extends Lightning.Component{

    static _template(){
        return {
            Image:{
                rect: true, w: 410, h: 230,
                shader: { type: Lightning.shaders.RoundedRectangle, radius: 15 },
            }
        }
    }

    set data(v){
        this.tag("Image").src = v.src;
    }

    set index(v){
        this._index = v;
    }

    _focus(){
        this.tag("Image").patch({
            smooth:{alpha:1, scale:1.1}
        });

    }

    _unfocus(){
        this.tag("Image").patch({
            smooth:{alpha:0.5, scale:1}
        });
    }
}