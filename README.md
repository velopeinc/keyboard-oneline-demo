# keyboard-oneline

## com.metrological.app.KeyboardOneline

#### Running the App

1. Install the NPM dependencies by running `npm install`

2. Build the App by running `lng build` inside the root of your project

3. Fire up a local webserver and open the App in a browser by running `lng serve` inside the root of your project

![keyboard](screenshot_1.png)
![keyboard](screenshot_2.png)