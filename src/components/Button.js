import {Lightning} from '@lightningjs/sdk';

export default class Button extends Lightning.Component {
    static _template() {
        return {
            w: 55, h: 60,
            Focus:{
                texture: Lightning.Tools.getRoundRect(
                    55, 60, 6, 0, 0, true, 0xffffffff
                ), alpha: 0
            },
            Label: {mount: 0.5, x: 30, y: 33, text: {fontSize:32}}
        };
    }

    set label(v) {
        this._label = v;
        this.tag("Label").text.text = v;

        if(v.length > 1){
            this.patch({
                w: 120,
                Focus:{
                    texture: Lightning.Tools.getRoundRect(
                        120, 60, 6, 0, 0, true, 0xffffffff
                    ),
                },
                Label:{
                    x: 60
                }
            })
        }
    }

    get label(){
        return this._label;
    }

    _focus() {
        this.patch({
            smooth:{scale:1.2},
            Focus:{
                smooth:{alpha:1}
            },
            Label:{
                smooth:{color:0xff000000}
            }
        });
    }

    _unfocus(){
        this.patch({
            smooth:{scale:1},
            Focus:{
                smooth:{alpha:0}
            },
            Label:{
                smooth:{color: 0xffffffff}
            }
        });
    }
}